package id.ac.ui.cs.advprog.MyAc.service;

import id.ac.ui.cs.advprog.MyAc.model.MatkulPlan;
import id.ac.ui.cs.advprog.MyAc.repository.MatkulPlanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class MatkulPlanServiceImplTest {
    @Mock
    private MatkulPlanRepository matkulPlanRepository;

    @InjectMocks
    private MatkulPlanServiceImpl matkulPlanServiceImpl;

    private MatkulPlan matkulPlan, matkulPlan2;

    @BeforeEach
    public void setUp() throws Exception {
        this.matkulPlan = new MatkulPlan(1L, 2L, "CS1234");
        this.matkulPlan = new MatkulPlan(2L, 3L, "CS5678");
    }

    @Test
    public void whenFindAllIsCalledItShouldCallMatkulPlanRepositoryFindAll() {
        matkulPlanServiceImpl.findAll();
        verify(matkulPlanRepository, times(1)).findAll();
    }

    @Test
    public void whenGetIsCalledItShouldCallMatkulPlanRepositoryFindById() {
        matkulPlanServiceImpl.findMatkulPlan(1L);
        verify(matkulPlanRepository, times(1)).findById(1L);
        matkulPlanServiceImpl.findMatkulPlan(2L);
        verify(matkulPlanRepository, times(1)).findById(2L);
    }

    @Test
    public void testErase() {
        matkulPlanServiceImpl.erase(1L);
        verify(matkulPlanRepository, times(1)).deleteById(1L);
    }

    @Test
    public void testRegister() {
        matkulPlanServiceImpl.register(matkulPlan);
        verify(matkulPlanRepository, times(1)).save(matkulPlan);
        matkulPlanServiceImpl.register(matkulPlan2);
        verify(matkulPlanRepository, times(1)).save(matkulPlan2);
    }

    @Test
    public void testRewrite() {
        matkulPlan.setKodeMatkul("CS00001");
        matkulPlanServiceImpl.rewrite(matkulPlan);
        verify(matkulPlanRepository, times(1)).save(matkulPlan);
    }
}
