package id.ac.ui.cs.advprog.MyAc.service;

import id.ac.ui.cs.advprog.MyAc.model.SemesterPlan;
import id.ac.ui.cs.advprog.MyAc.repository.SemesterPlanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class SemesterPlanServiceImplTest {
    @Mock
    private SemesterPlanRepository semesterPlanRepository;

    @InjectMocks
    private SemesterPlanServiceImpl semesterPlanServiceImpl;

    private SemesterPlan semesterPlan, semesterPlan2;

    @BeforeEach
    public void setUp() throws Exception {
        this.semesterPlan = new SemesterPlan(1L, 2L, 3);
        this.semesterPlan = new SemesterPlan(4L, 5L, 6);
    }

    @Test
    public void whenFindAllIsCalledItShouldCallSemesterPlanRepositoryFindAll() {
        semesterPlanServiceImpl.findAll();
        verify(semesterPlanRepository, times(1)).findAll();
    }

    @Test
    public void whenGetIsCalledItShouldCallSemesterPlanRepositoryFindById() {
        semesterPlanServiceImpl.findSemesterPlan(1L);
        verify(semesterPlanRepository, times(1)).findById(1L);
        semesterPlanServiceImpl.findSemesterPlan(2L);
        verify(semesterPlanRepository, times(1)).findById(2L);
    }

    @Test
    public void testErase() {
        semesterPlanServiceImpl.erase(1L);
        verify(semesterPlanRepository, times(1)).deleteById(1L);
    }

    @Test
    public void testRegister() {
        semesterPlanServiceImpl.register(semesterPlan);
        verify(semesterPlanRepository, times(1)).save(semesterPlan);
        semesterPlanServiceImpl.register(semesterPlan2);
        verify(semesterPlanRepository, times(1)).save(semesterPlan2);
    }

    @Test
    public void testRewrite() {
        semesterPlan.setSemester(4);
        semesterPlanServiceImpl.rewrite(semesterPlan);
        verify(semesterPlanRepository, times(1)).save(semesterPlan);
    }
}
