package id.ac.ui.cs.advprog.MyAc.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class LongPlanTest {

    private LongPlan longPlan;
    private LongPlan defaultLongPlan;

    @BeforeEach
    public void setUp() throws Exception {
        this.defaultLongPlan = new LongPlan();
        this.longPlan = new LongPlan(1L, 2L, "To be Great Spring Developer");
    }

    @Test
    public void testGetId() {
        assertEquals(1L, this.longPlan.getId());
    }

    @Test
    public void testGetKodeUser() {
        assertEquals(2L, this.longPlan.getKodeUser());
    }

    @Test
    public void testGetNamaLongPlan() {
        assertEquals("To be Great Spring Developer", this.longPlan.getNamaLongPlan());
    }

    @Test
    public void testSetNamaLongPlan() {
        this.longPlan.setNamaLongPlan("To be a Hacker");
        assertEquals("To be a Hacker", this.longPlan.getNamaLongPlan());
    }

    @Test
    public void testSetKodeUser() {
        this.longPlan.setKodeUser((long) 1);
        assertEquals(1, this.longPlan.getKodeUser());
    }

    @Test
    public void testSetId() {
        this.longPlan.setId((long) 2);
        assertEquals(2, this.longPlan.getId());
    }
}
