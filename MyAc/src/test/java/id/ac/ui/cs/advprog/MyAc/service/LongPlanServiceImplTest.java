package id.ac.ui.cs.advprog.MyAc.service;

import id.ac.ui.cs.advprog.MyAc.model.LongPlan;
import id.ac.ui.cs.advprog.MyAc.repository.LongPlanRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
public class LongPlanServiceImplTest {
    @Mock
    private LongPlanRepository longPlanRepository;

    @InjectMocks
    private LongPlanServiceImpl longPlanServiceImpl;

    private LongPlan longPlan, longPlan2;

    @BeforeEach
    public void setUp() throws Exception {
        longPlan = new LongPlan(1L, 2L, "To be Great Spring Developer");
        longPlan2 = new LongPlan(2L, 3L, "To be Hacker");
    }

    @Test
    public void whenFindAllIsCalledItShouldCallLongPlanRepositoryFindAll() {
        longPlanServiceImpl.findAll();
        verify(longPlanRepository, times(1)).findAll();
    }

    @Test
    public void whenGetIsCalledItShouldCallLongPlanRepositoryFindById() {
        longPlanServiceImpl.findLongPlan(1L);
        verify(longPlanRepository, times(1)).findById(1L);
        longPlanServiceImpl.findLongPlan(2L);
        verify(longPlanRepository, times(1)).findById(2L);
    }

    @Test
    public void testErase() {
        longPlanServiceImpl.erase(1L);
        verify(longPlanRepository, times(1)).deleteById(1L);
    }

    @Test
    public void testRegister() {
        longPlanServiceImpl.register(longPlan);
        verify(longPlanRepository, times(1)).save(longPlan);
        longPlanServiceImpl.register(longPlan2);
        verify(longPlanRepository, times(1)).save(longPlan2);
    }

    @Test
    public void testRewrite() {
        longPlan.setNamaLongPlan("To be Great Mobile Developer");
        longPlanServiceImpl.rewrite(longPlan);
        verify(longPlanRepository, times(1)).save(longPlan);
    }


}
