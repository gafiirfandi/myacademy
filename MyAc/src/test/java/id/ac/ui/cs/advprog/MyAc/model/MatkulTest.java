package id.ac.ui.cs.advprog.MyAc.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MatkulTest {

    private id.ac.ui.cs.advprog.MyAc.model.Matkul matkul;
    private id.ac.ui.cs.advprog.MyAc.model.Matkul defaultConstructor;

    @BeforeEach
    public void setUp() throws Exception {
        this.defaultConstructor = new id.ac.ui.cs.advprog.MyAc.model.Matkul();
        this.matkul = new id.ac.ui.cs.advprog.MyAc.model.Matkul("CS1234", "Matkul Test", "Deskripsi Test", 2, 5);
    }

    @Test
    public void testDefaultDummyIsNotNull() {
        assertNotNull(this.defaultConstructor);
    }

    @Test
    public void testGetKode() {
        assertEquals("CS1234", this.matkul.getKode());
    }

    @Test
    public void testGetNama() {
        assertEquals("Matkul Test", this.matkul.getNama());
    }

    @Test
    public void testGetPenjelasan() {
        assertEquals("Deskripsi Test", this.matkul.getPenjelasan());
    }

    @Test
    public void testGetSemester() {
        assertEquals(2, this.matkul.getSemester());
    }

    @Test
    public void testGetSks() {
        assertEquals(5, this.matkul.getSks());
    }

    @Test
    public void testToString() {
        assertEquals("Kode: CS1234, Nama: Matkul Test, Semester: 2, Sks: 5",
                this.matkul.toString());
    }
}

