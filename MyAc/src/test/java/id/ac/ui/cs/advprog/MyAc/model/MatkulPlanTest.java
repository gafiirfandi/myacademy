package id.ac.ui.cs.advprog.MyAc.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MatkulPlanTest {

    private MatkulPlan matkulPlan;
    private MatkulPlan defaultMatkulPlan;

    @BeforeEach
    public void setUp() throws Exception {
        this.defaultMatkulPlan = new MatkulPlan();
        this.matkulPlan = new MatkulPlan(1L, 2L, "CS1234");
    }

    @Test
    public void testGetId() {
        assertEquals(1L, this.matkulPlan.getId());
    }

    @Test
    public void testGetIdSemester() {
        assertEquals(2L, this.matkulPlan.getIdSemester());
    }

    @Test
    public void testGetKodeMatkul() {
        assertEquals("CS1234", this.matkulPlan.getKodeMatkul());
    }

    @Test
    public void testSetId() {
        this.matkulPlan.setId(2L);
        assertEquals(2L, this.matkulPlan.getId());
    }

    @Test
    public void testSetIdSemester() {
        this.matkulPlan.setIdSemester(3L);
        assertEquals(3L, this.matkulPlan.getIdSemester());
    }

    @Test
    public void testSetKodeMatkul() {
        this.matkulPlan.setKodeMatkul("Matkul kece");
        assertEquals("Matkul kece", this.matkulPlan.getKodeMatkul());
    }


}
