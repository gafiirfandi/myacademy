package id.ac.ui.cs.advprog.MyAc.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class SemesterPlanTest {

    private SemesterPlan semesterPlan;
    private SemesterPlan defaultSemesterPlan;

    @BeforeEach
    public void setUp() throws Exception {
        this.defaultSemesterPlan = new SemesterPlan();
        this.semesterPlan = new SemesterPlan(1L, 2L, 3);
    }

    @Test
    public void testGetId() {
        assertEquals(1L, this.semesterPlan.getId());
    }

    @Test
    public void testGetIdLong() {
        assertEquals(2L, this.semesterPlan.getIdLong());
    }

    @Test
    public void testGetSemester() {
        assertEquals(3, this.semesterPlan.getSemester());
    }

    @Test
    public void testSetId() {
        this.semesterPlan.setId(2L);
        assertEquals(2, this.semesterPlan.getId());
    }

    @Test
    public void testSetIdLong() {
        this.semesterPlan.setIdLong(3L);
        assertEquals(3, this.semesterPlan.getIdLong());
    }

    @Test
    public void testSetSemester() {
        this.semesterPlan.setSemester(4);
        assertEquals(4, this.semesterPlan.getSemester());
    }

}
