package id.ac.ui.cs.advprog.MyAc.controller;

import id.ac.ui.cs.advprog.MyAc.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = MatkulController.class)

public class MatkulControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @WithMockUser(username = "spring")
    @Test
    public void whenMatkulUrlAccessedShouldReturnMatkulPage() throws Exception {
        mockMvc.perform(get("/matkul"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));
    }

    @WithMockUser(username = "spring")
    @Test
    public void whenMatkulSearchUrlAccessedShouldReturnMatkulPage() throws Exception {
        mockMvc.perform(get("/matkul/search?matkul="))
                .andDo(print())
                .andExpect(status().is3xxRedirection())
                .andExpect(redirectedUrl("/matkul"));

        mockMvc.perform(get("/matkul/search?matkul=dsor"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));

        mockMvc.perform(get("/matkul/search?matkul=admin"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));

        mockMvc.perform(get("/matkul/search?matkul=&semester=1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));

        mockMvc.perform(get("/matkul/search?matkul=&sks=4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));

        mockMvc.perform(get("/matkul/search?matkul=&semester=1&sks=4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));

        mockMvc.perform(get("/matkul/search?matkul=dasar-dasar&semester=1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));

        mockMvc.perform(get("/matkul/search?matkul=aljabar&sks=3"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));

        mockMvc.perform(get("/matkul/search?matkul=dasar-dasar&semester=1&sks=4"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("matkulAll"))
                .andExpect(MockMvcResultMatchers.view().name("matkulSearch"));
    }

    @WithMockUser(username = "spring")
    @Test
    public void whenMatkulDetailUrlAccessedShouldReturnMatkulPage() throws Exception {

        mockMvc.perform(get("/matkul/details/{nama}", "Aljabar Linier"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.model().attributeExists("detailNama"))
                .andExpect(MockMvcResultMatchers.view().name("matkulDetail"));

    }
}
