package id.ac.ui.cs.advprog.MyAc.controller;

import id.ac.ui.cs.advprog.MyAc.model.LongPlan;
import id.ac.ui.cs.advprog.MyAc.model.MatkulPlan;
import id.ac.ui.cs.advprog.MyAc.model.SemesterPlan;
import id.ac.ui.cs.advprog.MyAc.model.User;
import id.ac.ui.cs.advprog.MyAc.repository.UserRepository;
import id.ac.ui.cs.advprog.MyAc.service.LongPlanService;
import id.ac.ui.cs.advprog.MyAc.service.MatkulPlanService;
import id.ac.ui.cs.advprog.MyAc.service.SemesterPlanService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Optional;

@Controller
@RequestMapping(path = "/longplan")
public class LongPlanController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    private LongPlanService longPlanService;

    @Autowired
    private SemesterPlanService semesterPlanService;

    @Autowired
    private MatkulPlanService matkulPlanService;

    @RequestMapping("")
    public String longPlan(Principal principal, Model model) {
        User user = userRepository.findByEmail(principal.getName());
        ArrayList<LongPlan> listLongPlan = new ArrayList<>();

        RestTemplate restTemplate = new RestTemplate();
        LongPlan[] arrLongPlan = restTemplate.getForObject("http://myacademia.herokuapp.com/api/longplan", LongPlan[].class);

        for(LongPlan longplan : arrLongPlan){
            if(longplan.getKodeUser().equals(user.getUser_id())) {
                listLongPlan.add(longplan);
            }
        }

        model.addAttribute("userId", user.getUser_id());
        model.addAttribute("listLongPlan", listLongPlan);
        return "longPlan";
    }

    @GetMapping("/edit")
    public String longPlanEdit(Principal principal, Model model) {
        return "longPlanEdit";
    }

    @GetMapping("/detail/{id}")
    public String longPlanDetail(Principal principal, @PathVariable Long id, Model model) {
        ArrayList<SemesterPlan> listSemesterPlan = new ArrayList<>();
        LongPlan longplan = longPlanService.findLongPlan(id).get();

        RestTemplate restTemplate = new RestTemplate();
        SemesterPlan[] arrSemesterPlan = restTemplate.getForObject("http://myacademia.herokuapp.com/api/semesterplan", SemesterPlan[].class);

        for(SemesterPlan semesterplan : arrSemesterPlan){
            if(semesterplan.getIdLong().equals(id)) {
                listSemesterPlan.add(semesterplan);
            }
        }

        model.addAttribute("longplan", longplan);
        model.addAttribute("listSemesterPlan", listSemesterPlan);
        return "longPlanDetail";
    }

    @GetMapping("/semesterplan/detail/{id}")
    public String semesterPlanDetail(Principal principal, @PathVariable Long id, Model model) {
        ArrayList<MatkulPlan> listMatkulPlan = new ArrayList<>();
        SemesterPlan semesterplan = semesterPlanService.findSemesterPlan(id).get();

        RestTemplate restTemplate = new RestTemplate();
        MatkulPlan[] arrMatkulPlan = restTemplate.getForObject("http://myacademia.herokuapp.com/api/matkulplan", MatkulPlan[].class);

        for(MatkulPlan matkulplan : arrMatkulPlan){
            if(matkulplan.getIdSemester().equals(id)) {
                listMatkulPlan.add(matkulplan);
            }
        }

        model.addAttribute("semesterplan", semesterplan);
        model.addAttribute("listMatkulPlan", listMatkulPlan);
        return "semesterPlanDetail";
    }


    @GetMapping("/create")
    public String longPlanCreate(Principal principal, Model model) {
        User user = userRepository.findByEmail(principal.getName());
        model.addAttribute("userId", user.getUser_id());
        LongPlan newlongplan = new LongPlan();
        model.addAttribute("longplan", newlongplan);
        return "longPlanCreate";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String longPlanCreate(@ModelAttribute("longplan") LongPlan longplan, Principal principal) {
        User user = userRepository.findByEmail(principal.getName());
        longplan.setKodeUser(user.getUser_id());
        longPlanService.register(longplan);
        return "redirect:/longplan";
    }

    @GetMapping("/semesterplan/create/{id}")
    public String semesterPlanCreate(Principal principal, @PathVariable Long id, Model model) {
        model.addAttribute("longId", id);
        SemesterPlan newsemesterplan = new SemesterPlan();
        model.addAttribute("semesterplan", newsemesterplan);
        return "semesterPlanCreate";
    }
    @RequestMapping(value = "/semesterplan/register/{id}", method = RequestMethod.POST)
    public String semesterPlanCreate(@ModelAttribute("semesterplan") SemesterPlan semesterplan, Principal principal,  @PathVariable Long id) {
        semesterplan.setIdLong(id);
        semesterPlanService.register(semesterplan);
        return "redirect:/longplan/detail/" + id;
    }

    @GetMapping("/matkulplan/create/{id}")
    public String matkulPlanCreate(Principal principal, @PathVariable Long id, Model model) {
        model.addAttribute("semesterId", id);
        MatkulPlan newmatkulplan = new MatkulPlan();
        model.addAttribute("matkulplan", newmatkulplan);
        return "matkulPlanCreate";
    }
    @RequestMapping(value = "/matkulplan/register/{id}", method = RequestMethod.POST)
    public String matkulPlanCreate(@ModelAttribute("matkulplan") MatkulPlan matkulplan, Principal principal,  @PathVariable Long id) {
        matkulplan.setIdSemester(id);
        matkulPlanService.register(matkulplan);
        return "redirect:/longplan";
    }


}
