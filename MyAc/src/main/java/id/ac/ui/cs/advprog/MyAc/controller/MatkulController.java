package id.ac.ui.cs.advprog.MyAc.controller;

import id.ac.ui.cs.advprog.MyAc.model.Matkul;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

@Controller
@RequestMapping(path = "/matkul")
public class MatkulController {

    /**
     * Routing for homepage matkul.
     * */
    @RequestMapping
    public String matkulHome(Model model) {

        RestTemplate restTemplate = new RestTemplate();
        Matkul[] listMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul", Matkul[].class);

        model.addAttribute("matkulAll", listMatkul);
        return "matkulSearch";

    }

    /**
     * Routing for search page matkul.
     * */
    @GetMapping("/search")
    public String find(@RequestParam(required = false) String matkul,
                       @RequestParam(required = false) String semester,
                       @RequestParam(required = false) String sks, Model model) {

        RestTemplate restTemplate = new RestTemplate();
        Matkul[] filteredMatkul = new Matkul[0];

        try {
            if (matkul.length() == 0 && semester == null && sks == null) {
                return "redirect:/matkul";
            } else if (matkul.length() != 0 && semester != null && sks != null) {
                filteredMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul" + "/semester="
                        + semester + "/nama=" + matkul + "/sks=" + sks, Matkul[].class);
            } else if (matkul.length() != 0 && semester == null && sks == null) {
                filteredMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul" + "/nama="
                        + matkul, Matkul[].class);
            } else if (matkul.length() == 0 && sks == null) {
                filteredMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul" + "/semester="
                        + semester, Matkul[].class);
            } else if (matkul.length() == 0 && semester == null) {
                filteredMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul" + "/sks="
                        + sks, Matkul[].class);
            } else if (matkul.length() == 0) {
                filteredMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul" + "/semester="
                        + semester + "/sks=" + sks, Matkul[].class);
            } else if (sks == null) {
                filteredMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul" + "/semester="
                        + semester + "/nama=" + matkul, Matkul[].class);
            } else if (semester == null) {
                filteredMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul" + "/sks="
                        + sks + "/nama=" + matkul, Matkul[].class);
            }

            model.addAttribute("matkulAll", filteredMatkul);
            return "matkulSearch";
        } catch (Exception e) {
            filteredMatkul = restTemplate.getForObject("http://matkulservice.herokuapp.com/matkul", Matkul[].class);
            model.addAttribute("matkulAll", filteredMatkul);
            return "matkulSearch";
        }
    }

    @RequestMapping("/details/{nama}")
    public String details(@PathVariable String nama, Model model) {
        model.addAttribute("detailNama", nama);
        return "matkulDetail";
    }
}
